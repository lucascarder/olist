from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView
from api.views import CategoryViewSet, CategoryUniqueViewSet, ChannelViewSet, testChannelUniqueViewSet
from django.conf import settings
from django.conf.urls import include

urlpatterns = [
    url(r'^category/?$', CategoryViewSet.as_view()),
    url(r'^category/(?P<category_slug>[A-y-0-9-]+)/?$', CategoryUniqueViewSet.as_view()),
    url(r'^channel/?$', ChannelViewSet.as_view()),
    url(r'^channel/(?P<channel_slug>[A-y-0-9-]+)/?$', testChannelUniqueViewSet.as_view()),
    url(r'^admin/', admin.site.urls),
    url(r'^$', TemplateView.as_view(template_name="index.html"))
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
