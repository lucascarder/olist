from .models import Category, Channel
from django.db.models import prefetch_related_objects
from rest_framework import serializers
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework_recursive.fields import RecursiveField
from mptt.templatetags.mptt_tags import cache_tree_children, tree_item_iterator, drilldown_tree_for_node


# Serializer for recursively retrieving all categories and it's subcategories with unlimited depth
class RootSerializer(serializers.ModelSerializer):
    children = serializers.SerializerMethodField()
    channel = serializers.ReadOnlyField(source='channel.name')

    def get_children(self, category):
        queryset = category.get_children()
        serialized_data = RootSerializer(queryset, many=True, read_only=True)
        return serialized_data.data

    class Meta:
        model = Category
        fields = ('name', 'slug', 'full_path', 'channel', 'children')


# Serializer for Category(s)
class UnnestedCategorySerializer(serializers.ModelSerializer):
    channel = serializers.ReadOnlyField(source='channel.name')

    class Meta:
        model = Category
        fields = ('name', 'slug', 'full_path', 'channel', 'children')


class testserializer(serializers.ModelSerializer):
    channel = serializers.ReadOnlyField(source='channel.name')
    #children = serializers.SerializerMethodField()

    def get_children(self, obj):
        queryset = obj.get_ancestors()
        serialized_data = testserializer(queryset, many=True, read_only=True, context=self.context)
        print(obj)
        return serialized_data.data

    class Meta:
        model = Category
        fields = ('name', 'slug', 'full_path', 'channel', 'children')


class TestUnnestedCategorySerializer(serializers.ModelSerializer):
    children = serializers.SerializerMethodField()
    channel = serializers.ReadOnlyField(source='channel.name')

    def get_children(self, obj):
        queryset = obj.get_ancestors()
        if obj.parent is not None:
            serialized_data = UnnestedCategorySerializer(queryset, many=True, read_only=True, context=self.context)
        else:
            serialized_data = RootSerializer(queryset, many=True, read_only=True, context=self.context)
        return serialized_data.data

    class Meta:
        model = Category
        fields = ('name', 'slug', 'full_path', 'channel', 'children')


class ParentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('name', 'slug', 'full_path')


class CategoryListSerializer(serializers.ModelSerializer):
    """
    Serialize Categories Tree
    """

    def to_representation(self, category):
        return category.generate_tree()


class UniqueCategorySerializer(serializers.ModelSerializer):
    parents = serializers.SerializerMethodField()
    channel = serializers.ReadOnlyField(source='channel.name')
    children = serializers.SerializerMethodField()

    def get_parents(self, category):
        queryset = category.get_ancestors()
        serialized_data = ParentSerializer(queryset, read_only=True, many=True)
        return serialized_data.data

    def get_children(self, category):
        queryset = category.get_children()
        serialized_data = RootSerializer(queryset, read_only=True, many=True)
        return serialized_data.data

    class Meta:
        model = Category
        fields = ('name', 'slug', 'full_path', 'channel', 'parents', 'children')


# Serializer for Channels
class ChannelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Channel
        fields = ('name', 'slug')


# A bit of a hack to fix Count field.
# By default, the count field will only count root nodes and not it's children
class CustomPagination(PageNumberPagination):
    def get_paginated_response(self, data):
        string_data = str(data)
        return Response({
            'pages': self.page.paginator.num_pages,
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'count': string_data.count('slug'),
            'results': data
        })
