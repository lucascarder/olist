from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from api.models import Category, Channel
import csv
from workatolist.settings import BASE_DIR
from time import process_time


class Command(BaseCommand):
    help = "Add categories to Channel from CSV file. Usage: manage.py importcategories <channel_name> <csv_file>"

    def add_arguments(self, parser):
        parser.add_argument('channel', type=str)
        parser.add_argument('csvfile', type=str)

    # Check if channel already exists.
    # If it does, remove all of it's categories and recreate. Otherwise, just create it.
    def handle(self, **options):
        try:
            channel = Channel.objects.get(name__iexact=options['channel'])  # Check if Channel already exists
            Category.objects.filter(channel=channel).delete()  # If it does, remove all of it's categories
            channel.delete()  # Then remove the channel itself
            new_channel = Channel.objects.create(name=options['channel']) # Recreate channel
        except ObjectDoesNotExist:
            new_channel = Channel.objects.create(name=options['channel']) # If the channel doesn't exist, simply create one

        self.readcsv(new_channel, options['csvfile']) # Call CSV Reading function passing channel name and csv file name

    # Reads csv, passes each row to a list sorted alphabetically, remove duplicates, and send each row to AddCategory
    def readcsv(self, new_channel, csvfile):
        with open(BASE_DIR+csvfile, newline='') as csvfile:  # Foward slash should be reconsidered if not running on WINDOWS
            reader = csv.DictReader(csvfile)
            category_list = []

            for row in reader:
                row = row['Category']
                row_cleaned = row.replace(' / ', '/')
                category_list.append(row_cleaned)

            for category in category_list:
                category_split = category.split('/')
                for i in range(len(category_split)):
                    subcategory = category.rsplit('/', 1)
                    if subcategory[0] not in category_list:
                        category_list.append(subcategory[0])

            category_list = set(category_list)  # Remove duplicates
            category_list = list(category_list)  # Turn into a list again
            category_list.sort()  # Sort list alphabetically before processing

            for category in category_list:
                self.addcategory(new_channel, category)

    # Check if category is a root and adds it accordingly
    def addcategory(self, new_channel, category):

        category_split = category.rsplit('/', 1)
        if len(category_split)> 1: # If it's not a root category
            try: # Check if category already exists in this channel
                category = Category.objects.get(full_path=category, channel=new_channel)
            except ObjectDoesNotExist: # If it doesn't exist, create it
                parent = Category.objects.get(full_path=category_split[0], channel=new_channel)
                category = Category.objects.create(name=category_split[1], channel=new_channel, parent=parent)
        else: # If it's a root category
            try: # Check if category already exists
                category = Category.objects.get(full_path=category_split[0], channel=new_channel)
            except ObjectDoesNotExist: # If it doesn't exist, create it
                category = Category.objects.create(name=category_split[0], channel=new_channel)

        self.stdout.write(category.full_path)