from .models import Category, Channel
from rest_framework import generics
from .serializers import RootSerializer, UniqueCategorySerializer, CategoryListSerializer, ChannelSerializer, testserializer, TestUnnestedCategorySerializer
from mptt.templatetags.mptt_tags import cache_tree_children, tree_item_iterator, drilldown_tree_for_node, get_cached_trees
from mptt.managers import TreeManager


# Returns all categories
class CategoryViewSet(generics.ListAPIView):
    queryset = cache_tree_children(Category.objects.all().select_related('channel'))  # gets all root nodes of categories
    serializer_class = RootSerializer


# Returns single category OR single category & it's parent categories and subcategories
'''class CategoryUniqueViewSet(generics.ListAPIView):
    serializer_class = CategoryListSerializer

    def get_queryset(self):
        category_slug = self.kwargs['category_slug']
        unique = self.request.query_params.get('unique', None)
        if unique is not None: # return single category if argument '?unique' is found in URL
            queryset = Category.objects.filter(slug__iexact=category_slug).select_related('channel')
        else: # returns category and it's parent categories and subcategories
            queryset = Category.objects.filter(slug__iexact=category_slug) # gets all categories related to the category requested. Result will not be nested
            #for a in tree_item_iterator(Category.objects.get(slug__iexact=category_slug).get_family()):
                #print(a)
        return queryset'''


# Returns single category OR single category & it's parent categories and subcategories
class CategoryUniqueViewSet(generics.ListAPIView):
    serializer_class = UniqueCategorySerializer

    def get_queryset(self):
        category_slug = self.kwargs['category_slug']
        queryset = Category.objects.filter(slug__iexact=category_slug).order_by('channel__name')
        return queryset

# Returns all channels
class ChannelViewSet(generics.ListAPIView):
    queryset = Channel.objects.all().order_by('name')  # gets all channels / order_by removes UnorderedObjectListWarning
    serializer_class = ChannelSerializer


# Returns all categories of a channel
class testChannelUniqueViewSet(generics.ListAPIView):
    serializer_class = RootSerializer

    def get_queryset(self):
        slug = self.kwargs['channel_slug']
        queryset = cache_tree_children(Category.objects.filter(channel__slug__iexact=slug).select_related('channel'))
        return queryset
