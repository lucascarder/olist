from django.db import models
from django.template.defaultfilters import slugify
from mptt.models import MPTTModel, TreeForeignKey  # Applies Modified Preorder Tree Traversal for performance and various utilities such as get_root()
import re
from time import process_time
from mptt.templatetags.mptt_tags import cache_tree_children, tree_item_iterator, drilldown_tree_for_node


class Channel(models.Model):
    name = models.CharField(max_length=50, blank=False)
    slug = models.SlugField(blank=True, unique=False, db_index=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = self.name.replace(' ', '-').lower()  # In case of space in Channel's name, that's why we need a SlugField
        super(Channel, self).save(*args, **kwargs)


class Category(MPTTModel):
    name = models.CharField(max_length=50, blank=False)
    channel = models.ForeignKey(Channel, null=False, blank=False, db_index=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)
    full_path = models.CharField(max_length=100, null=True, blank=True)
    slug = models.SlugField(blank=True, unique=True, db_index=True)

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        slug_str = "%s %s" % (self.name, self.channel)
        unique_slugify(self, slug_str)
        self.full_path = recursive(self) + self.name
        super(Category, self).save(*args, **kwargs)

    def generate_tree(self):
        result = {
            'name': self.name,
            'parents': [],
            'children': []
        }

        for category in self.get_ancestors():
            result['parents'].append(category.generate_json())
        print(self.get_children())
        result['children'].append(self.generate_children())

        '''for category in self.get_children():
            result['children'].append(category.generate_tree())

        root_result['children'].append(result)'''
        return result

    def generate_children(self):
        result = {
            'name': self.name,
            'children': []
        }

        for category in self.get_children():
            result['children'].append(category.generate_children())

        return result

    def generate_family(self):
        categories = drilldown_tree_for_node(self)
        result = {
            'name': self.name,
            'slug': self.slug,
            'children': []
        }
        print(categories)
        for category in categories:
            result['children'].append(category.generate_tree())
        return result

    def family(self):
        categories = list(tree_item_iterator(self.get_family()))
        result = categories[0][0].generate_json()
        level_count = len(categories[-1][1]['closed_levels'])
        print(categories)
        '''for i in range(len(categories)):
            category = categories[i][0]
            category['level'] = 100
        for i in range(level_count):
            print(categories)
        for a in filter(lambda category: category[0].name == 'Computers', categories):
            pass'''
        for i in range(len(categories), -1, -1):
            print(i)
            category = categories[i-1][0]
            new_level = categories[i-1][1]['new_level']
            if new_level is True:
                result = category.generate_json(result)
            else:
                result['children'].append(category.generate_json())

        return result

    def generate_json(self, *children):
        json = {
            'name': self.name,
            'slug': self.slug,
            'level': self.level,
        }

        return json


# Recursively turns parents into string. e.g: Computers/Hardware/CPUs/Intel
def recursive(category, path=""):
    if category.parent is None:
        return path
    else:
        path = category.parent.name + "/" + path
        return recursive(category.parent, path)


# From https://djangosnippets.org/snippets/690/
def unique_slugify(instance, value, slug_field_name='slug', queryset=None,
                   slug_separator='-'):
    """
    Calculates and stores a unique slug of ``value`` for an instance.
    ``slug_field_name`` should be a string matching the name of the field to
    store the slug in (and the field to check against for uniqueness).
    ``queryset`` usually doesn't need to be explicitly provided - it'll default
    to using the ``.all()`` queryset from the model's default manager.
    """
    slug_field = instance._meta.get_field(slug_field_name)

    slug = getattr(instance, slug_field.attname)
    slug_len = slug_field.max_length

    # Sort out the initial slug, limiting its length if necessary.
    slug = slugify(value)
    if slug_len:
        slug = slug[:slug_len]
    slug = _slug_strip(slug, slug_separator)
    original_slug = slug

    # Create the queryset if one wasn't explicitly provided and exclude the
    # current instance from the queryset.
    if queryset is None:
        queryset = instance.__class__._default_manager.all()
    if instance.pk:
        queryset = queryset.exclude(pk=instance.pk)

    # Find a unique slug. If one matches, at '-2' to the end and try again
    # (then '-3', etc).
    next = 2
    while not slug or queryset.filter(**{slug_field_name: slug}):
        slug = original_slug
        end = '%s%s' % (slug_separator, next)
        if slug_len and len(slug) + len(end) > slug_len:
            slug = slug[:slug_len-len(end)]
            slug = _slug_strip(slug, slug_separator)
        slug = '%s%s' % (slug, end)
        next += 1

    setattr(instance, slug_field.attname, slug)


def _slug_strip(value, separator='-'):
    """
    Cleans up a slug by removing slug separator characters that occur at the
    beginning or end of a slug.
    If an alternate separator is used, it will also replace any instances of
    the default '-' separator with the new separator.
    """
    separator = separator or ''
    if separator == '-' or not separator:
        re_sep = '-'
    else:
        re_sep = '(?:-|%s)' % re.escape(separator)
    # Remove multiple instances and if an alternate separator is provided,
    # replace the default '-' separator.
    if separator != re_sep:
        value = re.sub('%s+' % re_sep, separator, value)
    # Remove separator from the beginning and end of the slug.
    if separator:
        if separator != '-':
            re_sep = re.escape(separator)
        value = re.sub(r'^%s+|%s+$' % (re_sep, re_sep), '', value)
    return value
