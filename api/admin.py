from django.contrib import admin
from .models import Category, Channel


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'parent')

admin.site.register(Category, CategoryAdmin)
admin.site.register(Channel)